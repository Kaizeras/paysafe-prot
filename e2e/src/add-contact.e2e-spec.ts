import { browser, by, element } from 'protractor';

describe('Adding a contact', () => {

  it('Should make the form visible if we click on the "Add" button', () => {
    const button = element(by.id('add'));
    button.click();
    expect(element(by.id('contactsDetailsContainer')).isPresent()).toBeTruthy();
  });

  it('Should close the form on cancel', () => {
    const formCancelButton = element(by.id('contact-details__form__cancel-button'));
    browser.executeScript('arguments[0].scrollIntoView();', formCancelButton);
    formCancelButton.click();
    expect(element(by.id('contactsDetailsContainer')).isPresent()).toBeFalsy();
  });

  it('Should have a disabled "Add" button" ', () => {
    const button = element(by.id('add'));
    browser.executeScript('arguments[0].scrollIntoView();', button);
    button.click();
    const formContainer = element(by.id('contactsDetailsContainer'));
    const addButton     = formContainer.element(by.css('input[type="submit"]'));
    expect(addButton.getAttribute('disabled')).toBeTruthy();
  });

  it('Should fill out first name', () => {

    const formField = element(by.name('firstName'));
    formField.sendKeys('TestFirstName');

    expect(formField.getAttribute('value')).toContain('TestFirstName');
    expect(formField.getAttribute('class')).toContain('ng-valid');
  });

  it('Should fill out last name', () => {
    const formField = element(by.name('lastName'));
    formField.sendKeys('TestLastName');

    expect(formField.getAttribute('value')).toContain('TestLastName');
    expect(formField.getAttribute('class')).toContain('ng-valid');
  });

  it('Should throw an error if e-mail is invalid', () => {
    const formField = element(by.name('email'));
    formField.sendKeys('notanemail');

    expect(formField.getAttribute('value')).toContain('notanemail');
    expect(formField.getAttribute('class')).toContain('ng-invalid');

    formField.clear();
  });

  it('Should fill out e-mail correctly', () => {
    const formField = element(by.name('email'));
    formField.sendKeys('test@testuser.com');

    expect(formField.getAttribute('value')).toContain('test@testuser.com');
    expect(formField.getAttribute('class')).toContain('ng-valid');
  });


  it('Should have a enabled "Add" button" ', () => {
    const formContainer = element(by.id('contactsDetailsContainer'));
    const addButton     = formContainer.element(by.css('input[type="submit"]'));
    expect(addButton.getAttribute('disabled')).toBeFalsy();
  });

  it('Should submit the form', () => {
    const formContainer = element(by.id('contactsDetailsContainer'));
    const addButton     = formContainer.element(by.css('input[type="submit"]'));
    const list          = element(by.tagName('contacts-list')).all(by.tagName('li'));

    const initialListCount = list.count().then((initialCount) => {
      browser.executeScript('arguments[0].scrollIntoView();', addButton);
      addButton.click();
      const newListCount = list.count();
      expect(newListCount).toBeGreaterThan(initialCount);
    });
  });

});
