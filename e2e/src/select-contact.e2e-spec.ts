import { ContactsListPage } from './page-objects/contacts-list.po';
import { ContactDetailsPage } from './page-objects/contact-details.po';

describe('Select contact', () => {
  let contactsListPage: ContactsListPage;
  let contactDetailsPage: ContactDetailsPage;

  beforeAll(() => {
    contactsListPage   = new ContactsListPage();
    contactDetailsPage = new ContactDetailsPage();
  });

  it('Should select a contact', () => {

    // thirdContact.getAttribute('innerHTML').then((htmlContents) => {
    //   console.log(htmlContents);
    // });
    // const detailsContainer = element(by.id('contactsDetailsContainer'));

    contactsListPage.clickOnContact(2);
    // detailsContainer.getText().then((textContent) => {
    //   console.log(textContent);
    //   browser.debugger();
    // });
    expect(contactDetailsPage.container.getText()).toContain('Wallace');
  });
});
