import { by, element, ElementArrayFinder, ElementFinder, promise } from 'protractor';

export class ContactsListPage {
  public contactsList: ElementArrayFinder;

  constructor() {
    this.contactsList = element(by.tagName('contacts-list')).all(by.tagName('li'));
  }

  selectContact(index: number): ElementFinder {
    return this.contactsList.get(index);
  }

  clickOnContact(index: number): promise.Promise<void> {
    return this.contactsList.get(index).element(by.tagName('a')).click();
  }
}
