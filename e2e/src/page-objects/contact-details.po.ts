import { by, element, ElementFinder } from 'protractor';

export class ContactDetailsPage {
  public container: ElementFinder;
  public firstNameField: ElementFinder;
  public lastNameField: ElementFinder;
  public emailField: ElementFinder;

  constructor() {
    this.container      = element(by.id('contactsDetailsContainer'));
    this.firstNameField = this.container.element(by.name('firstName'));
    this.lastNameField  = this.container.element(by.name('lastName'));
    this.emailField     = this.container.element(by.name('email'));
  }
}
