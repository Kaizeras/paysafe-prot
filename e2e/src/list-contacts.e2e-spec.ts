import { browser, by, element } from 'protractor';
import { ContactsListPage } from './page-objects/contacts-list.po';

describe('Loading all contacts', () => {
  let contactsListPage: ContactsListPage;

  beforeAll(() => {
    browser.get('#');
    contactsListPage = new ContactsListPage();
  });

  it('Should show 5 contacts', () => {
    expect(contactsListPage.contactsList.count()).toEqual(5);
  });
});
